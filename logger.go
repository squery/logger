package logger

import (
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strconv"
	"time"

	"github.com/gookit/color"
)

type LogType string
type LogLevel string

const (
	logTypeError  = "ERROR"
	logTypeAccess = "ACCESS"
	logTypeInfo   = "INFO"

	logLevelDebug   = "Debug"
	logLevelRelease = "Release"
)

var (
	errorOut  io.Writer = os.Stderr
	infoOut   io.Writer = os.Stdout
	accessOut io.Writer = os.Stdout

	InfoLog   *Logger = NewInfo()
	ErrorLog  *Logger = NewError()
	AccessLog *Logger = NewAccess()
)

type Logger struct {
	Type LogType
	*log.Logger
}

func NewInfo() *Logger {
	return &Logger{
		Type:   logTypeInfo,
		Logger: log.New(infoOut, "", 0),
	}
}

func NewError() *Logger {
	return &Logger{
		Type:   logTypeInfo,
		Logger: log.New(errorOut, "", 0),
	}
}

func NewAccess() *Logger {
	return &Logger{
		Type:   logTypeInfo,
		Logger: log.New(accessOut, "", 0),
	}
}

type LogData struct {
	Time  time.Time
	Type  LogType
	Level LogLevel
	File  string
	Text  string
}

func newLog() LogData {
	return LogData{
		Time:  time.Now(),
		Type:  logTypeInfo,
		Level: logLevelDebug,
	}
}

func SetErrorOut(out io.Writer) {
	ErrorLog.SetOutput(out)
}

func SetInfoOut(out io.Writer) {
	InfoLog.SetOutput(out)
}

func SetAccessOut(out io.Writer) {
	AccessLog.SetOutput(out)
}

func build(logType LogType, text, file string) string {
	message := fmt.Sprintf("%s [%s]%s: %s\n", logType, time.Now().Format("2006/01/02 15:04:05"), file, text)
	return message
}
func setColor(logType LogType) LogType {
	var s string
	switch {
	case logType == logTypeInfo && infoOut == os.Stdout:
		s = color.FgGreen.Render(logTypeInfo)
	case logType == logTypeError && errorOut == os.Stderr:
		s = color.FgRed.Render(logTypeError)
	case logType == logTypeAccess && accessOut == os.Stdout:
		s = color.FgCyan.Render(logTypeAccess)
	}
	return LogType(s)
}

func filename(index int) string {
	_, filename, line, ok := runtime.Caller(index)
	if !ok {
		filename = "???"
		line = 0
	}
	filename = shortFilename(filename)
	return " " + filename + "." + strconv.Itoa(line)
}

func shortFilename(filename string) string {
	short := filename
	for i := len(filename) - 1; i >= 0; i-- {
		if filename[i] == '/' {
			short = filename[i+1:]
			break
		}
	}
	return short
}

func Info(text string) {
	file := filename(2)
	message := build(setColor(logTypeInfo), text, file)
	InfoLog.Writer().Write([]byte(message))
}

func Error(err error) {
	file := filename(2)
	message := build(setColor(logTypeError), err.Error(), file)
	ErrorLog.Writer().Write([]byte(message))
}

func Access(text string) {
	message := build(setColor(logTypeAccess), text, "")
	AccessLog.Writer().Write([]byte(message))
}
